/*
Kata : Mastermind

Ecrivez une fonction (mastermind).
L’utilisateur va devoir deviner une chaîne de trois lettres générée aléatoirement. Chacune de ces lettres est A, B, C, D ou E.
L’utilisateur a droit à trois tentatives.

    ok (l’utilisateur a bien deviné).
    ko avec le détail pour chaque lettres (Non si la lettre n’est pas dans la chaîne à trouver, presque si elle y est mais pas à l’emplacement indiqué, oui si la lettre indiquée est la bonne).
*/



function compare(inputArray){

    var randomArray    = [] ;
    var characters       = 'ABCDE';
    
    for ( let j = 0; j < 3; j++ ) {
       randomArray.push ( characters.charAt(Math.floor(Math.random() * 5)) );
    }
 
  console.log(randomArray);
 
  let res = [];
 
   for(let i=0;i<3;i++){
      
     if (inputArray[i]==randomArray[i]){
       res.push("oui");
     }
 
     else if (inputArray[i]!== randomArray[i]){
 
            if(randomArray.includes(inputArray[i])){
              res.push("presque");
            }
 
            else res.push("non");
     }
 
 
   }
  console.log(res);
 }
 
 
 compare(["E","B","A"]);
 
 
 
 