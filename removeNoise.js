/*

Kata : Messages cassée!!

Nous avons une machine à messages cassée qui introduit du bruit dans nos messages entrants.
 Nous savons que nos messages ne peuvent pas être écrits en utilisant %$&/#·@|º\ª et des 
 caractères blancs (comme des espaces ou des tabulations). Malheureusement, notre machine 
 introduit du bruit, ce qui signifie que notre message arrive avec des caractères comme: 
 %$&/#·@|º\ª dans notre message original.

Votre mission est d’écrire une fonction qui supprime ce bruit du message.

Notez que le bruit ne peut être que des caractères %$&/#·@|º\ª, les autres caractères ne sont 
pas considérés comme du bruit
Exemple

removeNoise("h%e&·%$·llo w&%or&$l·$%d") // returns hello world 

La fonction doit fonctionner avec ces entrées:

    %$&/#·@|º\ª
    he%$·ll@o c$&%odi%&ng for ev|#·ery&$$#$on%$·e
    c|o@$%de%w@a·$r%s &rºocªks

*/

// avec regex
function removeNoise(str){
    let reg = /([\%\$\&\/\#\·\@\|\º\ª\\])+/g;
    let newstr = str.replace(reg, ""); 
    console.log(newstr);
}

removeNoise(" %$&/#·@|º\ª");
removeNoise("he%$·ll@o c$&%odi%&ng for ev|#·ery&$$#$on%$·e");
removeNoise("c|o@$%de%w@a·$r%s &rºocªks");



//avec string et array method
function removeNoise2(test){
    
    let testArray = test.split("");
     
        function myFilter(letter){
        return letter != "%" 
        & letter != "$" 
        & letter != "&" 
        & letter !="/" 
        & letter !="#" 
        & letter !="·" 
        & letter != "@"
        & letter != "|"
        & letter != "º"
        & letter != "\\"
        & letter!= "ª";
            
        }
    let newArray = testArray.filter(myFilter);
    let newString = newArray.join("");
    console.log(newString);


}

removeNoise2(" %$&/#·@|º\ª");
removeNoise2("he%$·ll@o c$&%odi%&ng for ev|#·ery&$$#$on%$·e");
removeNoise2("c|o@$%de%w@a·$r%s &rºocªks");

// console.log(newArray);
// console.log(newString);
// console.log("\\");