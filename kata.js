//array filter, take out the element  which is not in the filter array

let data = [1,5,89,60];
let filter = [1,5];
let result = [];
for(i=0;i<data.length;i++){

    if (!filter.includes(data[i])){
         result.push(data[i]); //push() add elements to array;but returns the final array length after adding elements
    }

}
console.log(result);

//Kata :Leap year

// The tricky thing here is that a leap year in the 
// Gregorian calendar occurs: on every year that is evenly
//  divisible by 4 except every year that is evenly divisible by 100 unless the
//   year is also evenly divisible by 400 For example, 1997 is not a leap year, but
//    1996 is. 1900 is not a leap year, but 2000 is.

 //premiere façon
 const ans=1990;
 function leapyear(year)
 {
 return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
 }
console.log(leapyear(2016));
console.log(leapyear(2000));
console.log(leapyear(1700));
console.log(leapyear(1800));
console.log(leapyear(100));

console.log("==================");

//deuxieme façon
function leapYear(year){
    
    
    if (year%100===0){
       return year%400===0;}
        else return year%4===0;
    }
      


 console.log(leapYear(2016));
 console.log(leapYear(2000));
 console.log(leapYear(1700));
 console.log(leapYear(1800));
 console.log(leapYear(100));




 /**
  * Kata : chiffrement de César

Le principe
Créez une fonction pour déchiffrer, sans utiliser de boucle (grâce à ES6).

let déchiffrer = (message, decalage) => message_déchiffré;

Exemple : “Mv fd vh sdvvh elhq”, décalage de 3.
 * Applies a Caesar shift to all alphabetical characters in a string.
 * Characters are converted to uppercase in the returned string.
 * @param {int} key - Positions to shift (0-25).
 * @param {string} str - The string to shift.
 * @return {string} The shifted string.
 */
function caesarShift(str, key) {
    return str.toUpperCase().replace(/[A-Z]/g, c => String.fromCharCode((c.charCodeAt(0)-65+key)%26+65))
  }
  
  caesarShift("Mv fd vh sdvvh elhq",3);



// Kata : BigDiff

// Créer une fonction qui inverse les absisses et les ordonnées, par exemple :

// var tab1 = [
// 	["A", "B", "C"],
// 	[ 1,   2,   3 ],
// 	[true, false, false]
// ];

// devient :

// var tab2 = [
// 	[true,  1, "A"],  tab1[2][0], tab1[1][0], tab1[0][0],
// 	[false, 2, "B"],  tab1[2][1],tab1[1][1],tab1[0][1],
// 	[false, 3, "C"]   tab1[2][2],tab1[1][2],tab1[0][2],
// ];


var tab1 = [
	["A", "B", "C"],
	[ 1,   2,   3 ],
	[true, false, false]
];


function reversetab( array ){
    
      let result = [];

        for(let i = 0;i < array.length; i ++){

            let ligne = [];

        for(let j = array.length-1;j >= 0;j --){
            
            ligne.push(array[j][i]);
        }
            result.push(ligne);

        }

        return result;
}


let res = reversetab(tab1);

console.log(res);

