// Pour les plus valeureux :

// Give me a diamond !

// Vous devez renvoyer une chaîne qui ressemble à une forme de diamant lorsqu’elle est imprimée à l’écran, en utilisant des caractères astérisques (*). Les espaces de fin doivent être supprimés et chaque ligne doit être terminée par un caractère de nouvelle ligne (\n).

// Renvoie la valeur null / nil / None / ... si l’entrée est un nombre pair ou négatif, car il n’est pas possible d’imprimer un diamant de taille paire ou négative.

// Example :

function diamond(n){
    if(n<3 || n%2==0){
        return null;
    }
    let diamonds = [];
    function ajouterLigne(taille, etoiles){
        let ligne = new Array(taille);
        ligne.fill("*");
        let espaces = (taille - etoiles)/2;
        ligne.fill(" ", 0, espaces);
        ligne.fill(" ", taille-espaces, taille);
        return ligne;
}

    for(let i =1; i< n ; i+=2){
        diamonds.push(ajouterLigne(n, i));
    }
    for(let i =n; i>=1; i-=2){
        diamonds.push(ajouterLigne(n,i));
    }
    console.log(diamonds)
}

diamond(9);



